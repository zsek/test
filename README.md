# Small test repo

The repo has a simple "Hello World!" program in the following languages:

- Dockerfile.test	Small test Dockerfile 
- test.c	C test code
- test.php	PHP test code
- test.pl	Perl test code
- test.sh	Shell test code
