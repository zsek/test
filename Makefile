all:
	gcc -o test-c test.c

docker:
	docker build -t alpine-test -f Dockerfile.test .
	docker run -d --name alpine-test alpine-test

clean:
	-rm test-c
	-docker stop alpine-test
	-docker rm alpine-test
	-docker rmi-alpine-test
